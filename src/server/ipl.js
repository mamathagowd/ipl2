function matchesPerYear (matches)
{
 //returning total no of matches happened per year   
return matches.reduce((totalmatches,value) => {
if (totalmatches[value.season] === undefined) {
    totalmatches[value.season] = 1;
} else {
    totalmatches[value.season]++;
}
return totalmatches;
}, {});//creating an empty object to store matches per year
}
function matchesWonPerTeamPerYear(matches)
{    
    // returning no of matches won per team per year in IPL match
 return matches.reduce((wonteam,value) => {
     if (wonteam[value.season] === undefined) {
         wonteam[value.season] = {};
     } 
     //checking existance of  season and winner property and assigning the value
     wonteam[value.season][value.winner] = ((wonteam[value.season][value.winner]) || 0) + 1;
     return wonteam;
    // initializing the empty object 
 },{});
 
}
function extraRunsConcededPerTeamIn2016(matches,deliveries) {
    const ids = [];// to store the id values in the array
    matches.forEach((value) => {
     if(value.season === '2016') {
         ids.push(value.id);
     }
    }
    )
    // comparing the match id in the deliveries and ids in the array
const arr = deliveries.filter((value)=> {return ids.includes(value.match_id);})
const res = arr.reduce((extraruns,value) => {
extraruns[value.bowling_team] = ((extraruns[value.bowling_team]) || 0 ) +  parseInt(value.extra_runs);
    return extraruns;
 },{});
return res;
}
function Countbowlerwickets(matches,deliveries) {
    const bowlerdata =  deliveries.filter((value)=>{return value.bowler === 'TS Mills'});
    const bowlerids = bowlerdata.map((value)=>{return value.match_id});
    const res = matches.reduce((seasons,value)=>{
        if(bowlerids.includes(value.id)) {
            seasons[value.season] = ((seasons[value.season]) || []);
            seasons[value.season].push(value.id);
            return seasons;
        } else {
            return seasons;
        }
    },{});
    const bowlerarray = [];
    const seasons = Object.keys(res);
    seasons.forEach((values) => {
       const ids = res[values];
       const wickets = bowlerdata.reduce((bowlerwickets,value)=>{
           let count = 0;
           if(ids.includes(value.match_id)) {
               bowlerwickets[values] = ((bowlerwickets[values]) || 0);
               if ((value.player_dismissed).length === 0) {

               }
               else {
                bowlerwickets[values]++;
               }
           } 
           return bowlerwickets;
       },{});
       bowlerarray.push(wickets);
    })
    return bowlerarray;
}
function viratrunsperseason(matches,deliveries) {
    const viratdata = deliveries.filter((value)=>{return value.batsman === 'V Kohli'});
    const ids = viratdata.map((value)=>{
        return value.match_id;    });
    const res = matches.reduce((seasons,value)=>{
        if(ids.includes(value.id)) {
            seasons[value.season] = ((seasons[value.season]) || []);
              seasons[value.season].push(value.id); 
            return seasons;
        } else {
            return seasons;
        }
    },{});
    const array = [];
    const seasons = Object.keys(res);
    seasons.forEach((values)=>{
        const matchids = res[values];
        const score = viratdata.reduce((arrays,value)=> {
            if(matchids.includes(value.match_id)) {
                arrays[values] = (parseInt((arrays[values])) || 0) + parseInt(value.total_runs);
                return arrays;
            } else {
                return arrays;
            }
        },{});
        array.push(score);
    });
    return array;
    
}
function Top10EconomicalBowlersIn2015(matches,deliveries) {
    const season = {};
    const data = {};
    const bowler = {};
    const final = {};
    matches.filter(obj => obj.season === '2015').forEach((obj)=>{season[obj.id] = 1;
    })
    deliveries.filter((obj)=> obj.match_id in season).forEach((obj)=>{
        if(data[obj.bowler] === undefined) {
            data[obj.bowler] = {};
        }
        data[obj.bowler].runs = (data[obj.bowler].runs || 0 ) + parseInt(obj.total_runs) - parseInt(obj.legbye_runs) - parseInt(obj.bye_runs);
        if (!parseInt(obj.wide_runs) && !parseInt(obj.noball_runs)) {
           data[obj.bowler].overs = (data[obj.bowler].overs || 0 ) + (1 / 6);
        }
    }); 
    console.log(data);
    for (let k in data) {
        if (data[k].overs >= 2) {
            bowler[k] = data[k].runs / data[k].overs;
        }
    } 
    console.log(bowler);
    Object.entries(bowler).sort((a,b) => a[1] - b[1]).slice(0,10).forEach((arr)=>{
        return final[arr[0]] = parseFloat(arr[1].toFixed(2));
    }) 
    return final;
}
module.exports = {
    matchesPerYear,
    matchesWonPerTeamPerYear,
    extraRunsConcededPerTeamIn2016,
    Top10EconomicalBowlersIn2015,
    Countbowlerwickets,
    viratrunsperseason
};