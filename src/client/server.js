var http = require('http');
var fs = require('fs');

const server = http.createServer((req, res) => {
  switch (req.url) {
    case '/':
      fs.readFile('/home/mamatha/test/src/client/index.html', 'utf-8', (err, data) => {
        if (err) {
          console.error('Error : File not Found');
        } else {
          res.writeHead(200, {
            'Content-Type': 'text/html'
          })
          res.write(data);
          res.end();
        }
      });
      break;
    case '/matchesWon':
      fs.readFile('/home/mamatha/test/src/output/matchesWon.json', 'utf-8', (err, data) => {
        if (err) {
          console.error('Error : File not Found');
        } else {
          res.writeHead(200, {
            'Content-Type': 'text/html'
          })
          res.write(data);
          res.end();
        }
      });
      break;

    case '/bowlersEconomy':
      fs.readFile('/home/mamatha/test/src/output/bowlersEconomy.json', 'utf-8', (err, data) => {
        if (err) {
          console.error('Error : File not Found');
        } else {
          res.writeHead(200, {
            'Content-Type': 'text/html'
          })
          res.write(data);
          res.end();
        }
      });
      break;

    case '/extraRnsConceded':
      fs.readFile('/home/mamatha/test/src/output/extraRnsConceded.json', 'utf-8', (err, data) => {
        if (err) {
          console.error('Error : File not Found');
        } else {
          res.writeHead(200, {
            'Content-Type': 'text/html'
          })
          res.write(data);
          res.end();
        }
      });
      break;

    case '/matchesPerYear':
      fs.readFile('/home/mamatha/test/src/output/matchesPerYear.json', 'utf-8', (err, data) => {
        if (err) {
          console.error('Error : File not Found');
        } else {
          res.writeHead(200, {
            'Content-Type': 'text/html'
          })
          res.write(data);
          res.end();
        }
      });
      break;
      case '/app.js':
      fs.readFile('/home/mamatha/test/src/client/app.js', 'utf-8', (err, data) => {
        if (err) {
          console.error('Error : File not Found');
        } else {
          res.writeHead(200, {
            'Content-Type': 'text/html'
          })
          res.write(data);
          res.end();
        }
      });
      break;
  }
})
server.listen(3500);